const mysql = require('mysql');

const mysqlConnection = mysql.createConnection({
    host: 'localhost:3306',
    user: 'root',
    password: '',
    database: 'Gestion_de_rutas'
});

mysqlConnection.connect(function (err) {
    if(err) {
        console.log(err);
    } else {
        console.log('Db is connected');
    }
});

module.exports = mysqlConnection;